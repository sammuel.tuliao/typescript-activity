import * as assert from "assert";

function endZeros(a: number): number {
    let counter:number = 0;
    let number:any = a.toString();
    if(isNaN(a) || a < 0){
        return 0;
    }
    for(let index in number){
        if(number[index] == '0'){
            counter++;
        }
        else{
            counter=0;
        }

    }
    return counter;
}


console.log(endZeros(10));

// These "asserts" are used for self-checking
assert.strictEqual(endZeros(1), 0);
assert.strictEqual(endZeros(0), 1);
assert.strictEqual(endZeros(10), 1);
assert.strictEqual(endZeros(100), 2);
assert.strictEqual(endZeros(1000), 3);
